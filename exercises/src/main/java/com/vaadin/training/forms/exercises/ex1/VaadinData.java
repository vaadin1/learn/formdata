package com.vaadin.training.forms.exercises.ex1;

public class VaadinData {
    String email;
    String str;
    String vaadin;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }

    public String getVaadin() {
        return vaadin;
    }

    public void setVaadin(String vaadin) {
        this.vaadin = vaadin;
    }
}
