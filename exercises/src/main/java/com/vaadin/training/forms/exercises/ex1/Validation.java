package com.vaadin.training.forms.exercises.ex1;

import com.vaadin.flow.component.HasSize;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationResult;
import com.vaadin.flow.data.validator.EmailValidator;
import com.vaadin.flow.data.validator.StringLengthValidator;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteAlias;
import com.vaadin.training.forms.exercises.MainLayout;

import java.io.Serial;

@Route(value = Validation.ROUTE, layout = MainLayout.class)
@RouteAlias(value="", layout = MainLayout.class)
public class Validation extends VerticalLayout implements HasSize{

	@Serial
	private static final long serialVersionUID = 1L;

	public static final String ROUTE = "ex1";
	public static final String TITLE = "Validation";

	public Validation() {
		Binder<VaadinData> binder = new Binder<>(VaadinData.class);
		final TextField emailField = new TextField("Email validator");
		binder.forField(emailField)
				.withValidator(new EmailValidator("Are you sure the given value is an email address?"))
				.bind(VaadinData::getEmail, VaadinData::setEmail);

		final TextField stringField = new TextField("String length validator");
		binder.forField(stringField)
				.withValidator(new StringLengthValidator("Maximum of 10 characters allowed",0,10))
				.bind(VaadinData::getStr, VaadinData::setStr);

		final TextField vaadinField = new TextField("Vaadin validator");
		binder.forField(vaadinField)
						.withValidator(((value, context) -> {
							if (value == null || !value.equalsIgnoreCase("Vaadin"))
								return ValidationResult.error(value + " not accepted");
							else
								return ValidationResult.ok();
						}))
				.bind(VaadinData::getVaadin, VaadinData::setVaadin);
		add(emailField, stringField, vaadinField);
	}
}