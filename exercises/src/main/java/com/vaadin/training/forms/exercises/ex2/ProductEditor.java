package com.vaadin.training.forms.exercises.ex2;

import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.HasSize;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.html.Footer;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Consumer;

public class ProductEditor extends Composite<VerticalLayout> implements HasComponents, HasSize {
    private final Logger log = LoggerFactory.getLogger(ProductEditor.class);

    public ProductEditor(Product product, Consumer<Product> productConsumer) {
        // TODO Create a new class that extends a layout for editing the product
        final ProductFormLayout layout = new ProductFormLayout();

        // TODO Create a Binder and bind it together with the input fields
        // on the editor component you created. Note that after the bindings
        // have been defined, you should have the binder read the Product bean
        // given as a parameter.
        final Binder<Product> binder = new Binder<>(Product.class);
        binder.forField(layout.getNfPrice()).withConverter(new CurrencyConverter()).bind(Product::getPrice, Product::setPrice);
        binder.forField(layout.getDpAvail()).bind(Product::getAvailable, Product::setAvailable);
        binder.bindInstanceFields(layout);
        binder.readBean(product);

        // TODO Create a Save button which will write the values from the binder
        // to the Product bean. A successful save should also refresh the
        // read-only view
        final HorizontalLayout footer = new HorizontalLayout();
        footer.add(new Button("Save", event -> {
            try {
                binder.writeBean(product);

                productConsumer.accept(product);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }));
        // TODO Create a Cancel button which will read the values from the
        // Product bean to the binder
        footer.add(new Button("Cancel", event -> binder.readBean(product)));
        footer.getThemeList().set("spacing", true);

        add(layout, footer);
    }
}
