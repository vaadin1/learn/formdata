package com.vaadin.training.forms.exercises.ex2;

import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.PropertyId;

public class ProductFormLayout extends FormLayout {
    @PropertyId("name")
    final private TextField tName = new TextField("Name");
    final private TextField nfPrice = new TextField("Price");
    final private DatePicker dpAvail = new DatePicker("Available");

    public ProductFormLayout() {
        add(tName, nfPrice, dpAvail);
    }

    public TextField gettName() {
        return tName;
    }

    public TextField getNfPrice() {
        return nfPrice;
    }

    public DatePicker getDpAvail() {
        return dpAvail;
    }
}
