package com.vaadin.training.forms.exercises.ex2;

import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.HasSize;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ReadOnlyHasValue;

import java.time.LocalDate;

public class ProductViewer extends Composite<FormLayout> implements HasSize {
    private final Paragraph pName = new Paragraph();
    private final Paragraph pPrice = new Paragraph();
    private final Paragraph pAvail = new Paragraph();
    private final Binder<Product> binder = new Binder<>(Product.class);
    public ProductViewer(Product product) {
        final FormLayout layout = getContent();
        layout.addFormItem(pName, new Span("Name"));
        layout.addFormItem(pPrice, new Span("Price"));
        layout.addFormItem(pAvail, new Span("Available"));

        binder.forField(new ReadOnlyHasValue<>(pName::setText)).bind(Product::getName, null);
        binder.forField(new ReadOnlyHasValue<Double>(price -> pPrice.setText(String.valueOf(price)))).bind(Product::getPrice, null);
        binder.forField(new ReadOnlyHasValue<LocalDate>(avail -> pAvail.setText(avail.toString()))).bind(Product::getAvailable, null);

        refresh(product);
    }

    void refresh(Product product) {
        binder.readBean(product);
    }
}
